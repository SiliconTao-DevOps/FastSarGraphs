#!/usr/bin/perl

while (<>)
{
	@data = split(/\t/);
	#print "data[0]='$data[0]'\tdata[1]='$data[1]'\n";
	if($data[0] =~ m/^\d/)
	{		
		chomp($data[1]);
		$data[0] =~ m/^(\d\d:\d\d:)/;
		$AdjustTime = $1."01";
		$Times{$AdjustTime} = 1;
		#print "AdjustTime = '$AdjustTime'\n";
		$Result{$Server}{$AdjustTime} = $data[1];
		#sleep(1);
	} else
	{
		chomp($data[0]);
		$Servers{$data[0]} = 1;
		$Server = $data[0];
	}
}

printf "Time     \t";
foreach $S (sort(keys(%Servers)))
{
	printf "$S\t";
}
print "\n";

foreach $T (sort(keys(%Times)))
{
	print "$T\t";
	foreach $S (sort(keys(%Servers)))
	{
		# printf "$Result{$S}{$T}\t";
		#printf "%0.2f\t", ((defined($Result{$S}{$T}))?( $Result{$S}{$T} ):(100.00)) 
		if(defined($Result{$S}{$T}))
		{
			printf "%0.2f\t", $Result{$S}{$T};
			$Result{$S}{"LAST"} = $Result{$S}{$T};
		} else
		{
			if(! defined($Result{$S}{"LAST"}))
			{
				$Result{$S}{"LAST"} = 100.00;
			}
			printf "%0.2f\t", $Result{$S}{"LAST"};			
		}
		#sleep(1);
	}
	print "\n";
	#sleep(1);
}

