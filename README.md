
This project began with the need to quickly turn log files and SAR data into charts.

There are three main tools for admins to use.
1. Gbt.pl will "**Group By Time**" the data so that meaningful data can be inconsistent columns and rows.
2. plottoer.pl will generate generic plots from simple data sets.
3. FastSarGraphs.sh will scan a target system for SAR report data and build specially formatted SAR graphs.

# Gbt
This can perform various operations on integer and floating point fields. It can also make the first column of type time, organized in increments that will allow the data to be programmatically integrated with data from other sets, aligning on a common time stamp.

The default time step is ten minutes. This can be changed by using the argument ``-s`` followed by an integer like 1, 5, 30, or any number less than 60.

Output data columns can be formatted using ``-f`` use the following operations
- max	: Maximum value in range
- sum 	: summation of all values in range
- min	: Minimum values in range
- del 	: Delta, difference from last value
- mean	: Average values in range
- std 	: Standard deviation
- count	: Count of samples

Multiple output columns can be defined in the format by separating with a colon.

Example
```bash
cat sample2.dat | ./Gbt.pl -f min:mean:max
```
![sample2.jpg](sample2.jpg "Sample 2")

# Plotter
Quickly generate plots from simple data sets.

Example
```bash
cat sample1.dat|./plotter.pl -t "The Title" -o sample1.png
```
![sample1.png](sample1.png "Sample 1")

# FastSarGraphs
Fast Sar Graphs quickly generates gnuplot files from ``System Activity Reporter`` data, then generates the graphs from those plot files. It also generates an Idle vs. Load chart and if given multiple targets will produce a pool load chart to compare systems within the pool.

## Example Use

```bash
./FastSarGraph.sh CrashBot -d 3 -s 03:00:00 -e 04:00:00 -t Test -l root
```
![CrashBot_general_Test.png](CrashBot_general_Test.png "SAR graph")

![CrashBot_IdleVsLoad_Test.png](CrashBot_IdleVsLoad_Test.png "Idle vs Load")

## Installing SAR

SAR should be run frequently to collect data. One way to do this is to set cron to run ``sysstat``

Example ``/etc/cron.d/sysstat``
```cron
# run system activity accounting tool every 10 minutes
*/10 * * * * root /usr/lib64/sa/sa1 -d 1 1

# generate a daily summary of process accounting at 23:53
53 23 * * * root /usr/lib64/sa/sa2 -A
```


### Installing For Red Hat, CentOS & Fedora
```bash
yum install sysstat
```


# Installing Gnuplot & Perl Libraries
The scripts can be run directly from the git clone directory. Some dependencies are needed.

## Dependencies for Red Hat
```bash
yum install perl-DateTime-Format-Strptime gnuplot
```

## Dependencies for Debian
```bash
apt-get install libdatetime-format-strptime-perl gnuplot
```

## Dependencies for Mac OS
```bash
brew install gnuplot
```

