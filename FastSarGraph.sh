#!/bin/bash

COMMAND="$@"

# exit if child process errors
set -e

DEBUG=

if [ "$1" = "" ]; then
	echo "You must supply a target hostname to query SAR data from."
	echo "example: $0 web1.example.com"
	exit 42
fi

SSH_OPTS="-oPasswordAuthentication=no -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no"

# Field names and indexes
function GetIndexOfField {
	FIELD_NAME=${1%%:*}
	IdField=$(head -n 1 ${TMP_DATA} | sed -e 's/  */\n/g'|grep -nv ^$|grep ":${FIELD_NAME}"$) || {
		FIELD_NAME=${1##*:}
		IdField=$(head -n 1 ${TMP_DATA} | sed -e 's/  */\n/g'|grep -nv ^$|grep ":${FIELD_NAME}"$) || {
			echo "No such field '$1'" >&2;
			exit $LINENO;
		}
	}
	echo ${IdField%%:*}
}

function BuildDatFile {
	echo "BuildDatFile from $TMP_DATA"
	NewDatFile=$(mktemp ${TMP_NAME}_XXXXXXXXXXXXXX)

	PrintCmd=""
	while [ $# -gt 0 ]; do
		PrintCmd="$PrintCmd,\$"$(GetIndexOfField "$1")
		# echo "PrintCmd="$PrintCmd
		shift
	done
	#echo "head $TMP_DATA | gawk '{print \$1'$PrintCmd'}'"
	cat $TMP_DATA | gawk "{print \$1$PrintCmd}" > ${NewDatFile}_TITLES
	cat ${NewDatFile}_TITLES | grep ^[0-9] | sort -n > $NewDatFile
}

function ShowHelp {
	echo "Options:
-n <pool name> # When giving more then one hostname
-d <scan date, two digit integer>
-s <start time, HH:MM:SS>
-e <end time, HH:MM:SS>
-w # no image viewer, for scripting
-l <user login name>
-t <title of graphs>
-D # Debug with verbose information"
	exit 17
}

TARGETS=""
SCAN_DATE=
START_TIME=
END_TIME=
POOL_NAME=
USER_LOGIN=
WSI=0

while [ $# -gt 0 ]; do
	case $1 in
		-s) shift; START_TIME="$1";;
		-e) shift; END_TIME="$1";;
		-n) shift; POOL_NAME="$1";;
		-l) shift; USER_LOGIN="-l $1";;
		-d) shift; SCAN_DATE=$(printf "%d" $1);;
		-w) WSI=1;;
		-t) shift; TITLE="$1";;
		-h) ShowHelp;;
		-D)	DEBUG=1;;
		*) TARGETS="$TARGETS $1"
	esac
	shift
done

((DEBUG == 1)) && {
	echo "Set debug"
	DEBUG_V="-v"
}

[ ${#START_TIME} -gt 1 ] && {
	[ ${#START_TIME} -ne 8 ] && {
		echo "Start/End time must be in the format of HH:MM:SS"
		exit $LINENO
	}
}
[ ${#END_TIME} -gt 1 ] && {
	[ ${#END_TIME} -ne 8 ] && {
		echo "Start/End time must be in the format of HH:MM:SS"
		exit $LINENO
	}
}


REPORT_TIME=$(date +%F-%H%M%S)
RUN_PATH=$(dirname $0)
(( DEBUG == 1)) && echo cd $RUN_PATH
cd $RUN_PATH
STOW_PATH=$HOME/Pictures/FastSarGraphs/
[ $WSI -eq 1 ] && STOW_PATH=$RUN_PATH/STOW/
TARGET_COUNT=0
DATE=$(date +%F)
for TARGET in $TARGETS; do
	TARGET_COUNT=$(($TARGET_COUNT + 1))
	TMP_NAME=$(mktemp /tmp/SAR_${TARGET}_XXXXXXXXXXXXXXX)
	TMP_DATA="${TMP_NAME}.dat"
	(( DEBUG == 1)) && echo "TMP_DATA='$TMP_DATA'"
	TMP_REPORT="${TMP_NAME}.plt"
	TMP_PNG="${TMP_NAME}.png"
	TMP_CPU_PNG="${TMP_NAME}_CPU.png"

	# Get the RAW SAR output
	(( DEBUG == 1)) && echo "cat report.tmp | ssh $SSH_OPTS $USER_LOGIN $TARGET"
	cat remote.report.sh | sed -e "s/^REPLACE_START=.*/REPLACE_START=\"$START_TIME\"/" > report.tmp
	sed -i report.tmp -e "s/^REPLACE_END=.*/REPLACE_END=\"$END_TIME\"/"
	sed -i report.tmp -e "s/^REPLACE_DAY=.*/REPLACE_DAY=\"$SCAN_DATE\"/"
	sed -i report.tmp -e "s/^DEBUG=.*/DEBUG=$DEBUG/"
	cat report.tmp | ssh $SSH_OPTS $USER_LOGIN $TARGET "bash -" > ${TMP_DATA}.raw
	REPORT_DATE=$(head ${TMP_DATA}.raw | grep ^DATE | cut -d= -f2)
	#head ${TMP_DATA}.raw
	cat ${TMP_DATA}.raw | sed -e '1,/BEGIN DATA/d' > ${TMP_DATA}.src

	(( DEBUG == 1)) && echo "${TMP_DATA}.src"

	# Build single unified table of all data
	cat ${TMP_DATA}.src | gawk -f dynamic.report.awk > $TMP_DATA

	[ ! -d "/dev/shm/DEBUG_FSG" ] && {
		mkdir /dev/shm/DEBUG_FSG
		cp ${TMP_DATA}* /dev/shm/DEBUG_FSG
	}

	# Create the general report
	# remove intr/s
	#BuildDatFile "%user" "%system" "%iowait" "%idle" "%memused" "kbbuffers" "kbcached" "%swpused" "pswpin/s" "pswpout/s" "runq-sz" "ldavg-1" "intr/s" "proc/s" "cswch/s" "tps" "rtps" "wtps"
	BuildDatFile "%user:%usr" "%system:%sys" "%iowait:%wait" "%idle" "%memused:slabmem" "kbbuffers:buffers" "kbcached:cached" "kbmemfree:memfree" "kbmemused:slabmem" "%swpused:swpfree" "pswpin/s" "pswpout/s" "runq-sz" "ldavg-1" "proc/s" "cswch/s" "tps" "rtps" "wtps"
	cp report.tplt $TMP_REPORT
	USE_TITLE=
	sed -i $TMP_REPORT -e "s|output.png|${TARGET}_general_${TITLE}.png|";
	sed -i $TMP_REPORT -e "s/TITLE_NAME/${TARGET}/";
	sed -i $TMP_REPORT -e "s/COMMENT/${REPORT_DATE}/";
	sed -i $TMP_REPORT -e "s|IN_DATA|${NewDatFile}|";
	gnuplot < ${TMP_REPORT}

	BuildDatFile "%idle" "ldavg-1"
	cp IdleVsLoad.tplt $TMP_REPORT
	sed -i ${TMP_REPORT} -e "s|output.png|${TARGET}_IdleVsLoad_${TITLE}.png|";
	sed -i $TMP_REPORT -e "s/TITLE_NAME/${TARGET}/";
	sed -i ${TMP_REPORT} -e "s|IN_DATA|${NewDatFile}|";
	sed -i ${TMP_REPORT} -e "s|COMMENT|${REPORT_DATE}|";
	gnuplot < ${TMP_REPORT}

	mkdir $DEBUG_V -p $STOW_PATH/${REPORT_TIME}

	BuildDatFile "rxpck/s" "txpck/s"
	mv $DEBUG_V ${NewDatFile} $STOW_PATH/${REPORT_TIME}/${TARGET}.net.data

	BuildDatFile "%user:%usr" "%system:%sys" "%iowait:%wait" "%idle"
	mv $DEBUG_V ${NewDatFile} $STOW_PATH/${REPORT_TIME}/${TARGET}.cpu.data

	BuildDatFile "rtps" "wtps"
	mv $DEBUG_V ${NewDatFile} $STOW_PATH/${REPORT_TIME}/${TARGET}.disk.data

	BuildDatFile "kbmemused:slabmem"
	mv $DEBUG_V ${NewDatFile} $STOW_PATH/${REPORT_TIME}/${TARGET}.mem.data

	cp $DEBUG_V ${TMP_DATA}* $STOW_PATH/${REPORT_TIME}
	mv $DEBUG_V *.png $STOW_PATH/${REPORT_TIME}
	# cp ${TMP_DATA}.raw $STOW_PATH/${REPORT_TIME}
	rm $DEBUG_V -f ${TMP_NAME}*
done

echo "${COMMAND}" > $STOW_PATH/${REPORT_TIME}/command

[ "${POOL_NAME}x" = "x" ] && {
	POOL_NAME="SAR"
}

# Pool Load report if given multiple hosts to report on
if [ $TARGET_COUNT -gt 1 ]; then
	echo "Generate Pool graph"
	(( DEBUG == 1)) && echo "mktemp /tmp/${POOL_NAME}_POOL_XXXXXXXXXXXXXXXX"
	TMP_NAME=$(mktemp /tmp/${POOL_NAME}_POOL_XXXXXXXXXXXXX)
	REPORT_HOSTS=""
	for DF in $STOW_PATH/${REPORT_TIME}/*.dat; do
		TMP_DATA=$DF
		BuildDatFile "%idle" "ldavg-1"
		echo "===========" >> $TMP_NAME
		THIS_HOST=$(basename $DF | sed -e 's/.*_\(.*\)_.*/\1/')
		REPORT_HOSTS="$REPORT_HOSTS $THIS_HOST"
		sed -i ${NewDatFile}_TITLES -e "s/%idle/${THIS_HOST}_%idle/"
		sed -i ${NewDatFile}_TITLES -e "s/ldavg-1/${THIS_HOST}_ldavg-1/"
		sed -i ${NewDatFile}_TITLES -e 's/^\(..:..:\)../\100/'
		cat ${NewDatFile}_TITLES >> $TMP_NAME
		rm $DEBUG_V -f ${NewDatFile}*
	done
	cat $TMP_NAME | gawk -f dynamic.report.awk > ${TMP_NAME}.src
	TMP_DATA=${TMP_NAME}.src
	cp pool_load.tplt ${TMP_NAME}.plt
	for h in $REPORT_HOSTS; do
		sed -i ${TMP_NAME}.plt -e 's/EOL_MARK.*/, \\/'
		FieldIndex=$(GetIndexOfField "${h}_%idle")
		sed -i ${TMP_NAME}.plt -e "s/^# MARK IDLE/  'IN_DATA' using 1:(100 - \$$FieldIndex) with lines title '$h'EOL_MARK\n# MARK IDLE/"
		FieldIndex=$(GetIndexOfField "${h}_ldavg-1")
		sed -i ${TMP_NAME}.plt -e "s/^# MARK LOAD/  'IN_DATA' using 1:$FieldIndex with lines title '$h'EOL_MARK\n# MARK LOAD/"
	done
	sed -i ${TMP_NAME}.plt -e 's/EOL_MARK.*/\n/'
	tail -n +2 ${TMP_NAME}.src | sort -n > ${TMP_NAME}.dat
	sed -i ${TMP_NAME}.plt -e "s|output.png|${TMP_NAME}_PoolLoad.png|";
	sed -i ${TMP_NAME}.plt -e "s|IN_DATA|${TMP_NAME}.dat|";
	sed -i ${TMP_NAME}.plt -e "s|COMMENT|${REPORT_DATE}|";
	gnuplot < ${TMP_NAME}.plt
	mv $DEBUG_V ${TMP_NAME}_PoolLoad.png $STOW_PATH/${REPORT_TIME}
	rm -f ${TMP_NAME}*
fi

if [ $WSI -eq 1 ]; then
	echo "Reports are in $STOW_PATH/${REPORT_TIME}"
else
	# Stop the stupid GNOME bug that will never get fixed from filling the screen with garbage.
	gthumb $STOW_PATH/${REPORT_TIME}/ 2>&1 | grep -vE "Source ID .* was not found when attempting to remove it|^$"
fi



