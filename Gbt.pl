#!/usr/bin/perl

use DateTime;
use DateTime::Format::Strptime qw( );

my $TimeStepMinutes = 10;
my $Format = "max";
my $DeltaLastValue = 0;
my $DeltaLastAnswer = 0;
my $DeltaSum = 0;
my $Debug = 0;

sub ShowHelp {
	print" Gbt.pl will take columns of numbers and calculate the min/mean/max within a range of time from the first column.
The range of time is a block of minutes, the default is a 10 minute block. Within that block of time all numbers from each column after the first will be calculated for min/mean/max.

-s <minutes> : Set the time step size. Default is 10. To step by hours use multipes of 60 minutes.
-h : Show help screen and exit
-d : Enable debug mode for verbose messages of confusion.
-f <format> : Select the output format fields seperated by a colon.
		Fields:
			max		: Maximum value in range 
			sum 	: sumation of all values in range 
			min		: Minimum values in range 
			del 	: Delta, difference from last value
			mean	: Average values in range (not working yet, just planning)
			std 	: Standard deviation (not working yet, just planning)
			count	: Count of samples
		example -f max:total
\n";
	exit;
}

sub CalculateDel {
	my $Fieldname = shift @_;
	my $WorkingValue = shift @_;
	my $NextValue = shift @_;
	#print "CalcMax: Fieldname=$Fieldname\tWorkingValue='$WorkingValue'\tNextValue='$NextValue'\tDeltaSum='$DeltaSum'\tDeltaLastValue='$DeltaSum'\tDeltaLastAnswer='$DeltaLastAnswer'\n";
	if($WorkingValue eq "") {
		$DeltaSum = $NextValue;
		$DeltaLastValue = $DeltaLastAnswer;
		# $WorkingValue = $NextValue;
		#print "DeltaLastValue = DeltaLastAnswer '$DeltaLastAnswer'\n";
	} # else {		
		$DeltaSum = $NextValue;
		if($DeltaSum > $DeltaLastValue) {
			$WorkingValue = $DeltaSum - $DeltaLastValue;
		}
		if($DeltaSum < $DeltaLastValue) {
			$WorkingValue = $DeltaLastValue - $DeltaSum;
		}
		$DeltaLastAnswer = $DeltaSum;
	# }
	#print "WorkingValue='$WorkingValue'\t Answer = '$DeltaSum'\n";
	return($WorkingValue);
}

sub CalculateMax {
	my $Fieldname = shift @_;
	my $WorkingValue = shift @_;
	my $NextValue = shift @_;
	#print "CalcMax: Fieldname=$Fieldname\tWorkingValue='$WorkingValue'\tNextValue='$NextValue'\n";
	if($WorkingValue eq "") {
		$WorkingValue = $NextValue;
	}
	if($WorkingValue < $NextValue) {
		#print "replace orkingValue='$WorkingValue'\tNextValue='$NextValue'\n";
		$WorkingValue = $NextValue;
	}
	return($WorkingValue);
}

sub CalculateSum {
	my $Fieldname = shift @_;
	my $WorkingValue = shift @_;
	my $NextValue = shift @_;
	#print "CalcSum: Fieldname=$Fieldname\tWorkingValue=$WorkingValue\tNextValue=$NextValue\n";
	if($WorkingValue eq "") {
		$WorkingValue = $NextValue;
	} else {
		$WorkingValue += $NextValue;
	}
	return($WorkingValue);
}

sub CalculateMin {
	my $Fieldname = shift @_;
	my $WorkingValue = shift @_;
	my $NextValue = shift @_;
	if($WorkingValue eq "") {
		$WorkingValue = $NextValue;
	}
	if($WorkingValue gt $NextValue) {
		$WorkingValue = $NextValue;
	}
	return($WorkingValue);
}

sub CalculateStd {
	printf "line %d is not working yet.\n", __LINE__; 
}

sub Calculate {
	my $NextValue = shift @_;
	my $ContainerRef = shift @_;
	# print "Format=$Format\tContainerRef=$ContainerRef\tNextValue=$NextValue\n";
	my %WorkingHash;
	@WorkingHash{split(/:/,$Format)} = split(/:/,$ContainerRef);
	#foreach $w (keys %WorkingHash) {
	#	print "w='$w'\t WorkingHash->{$w}='".$WorkingHash->{$w}."'\n";
	#}
	$WorkingHash->{"count"}++;
	$WorkingHash->{"sum"} += $NextValue;
	$WorkingHash->{"mean"} = $WorkingHash->{"sum"} / $WorkingHash->{"count"};

	my @Fields = split(/:/,$Format);
	foreach (@Fields) {
		if($_ eq "max") 	{ $WorkingHash->{$_} = CalculateMax($_, $WorkingHash->{$_}, $NextValue); }
		if($_ eq "min") 	{ $WorkingHash->{$_} = CalculateMin($_, $WorkingHash->{$_}, $NextValue); }
		if($_ eq "std") 	{ $WorkingHash->{$_} = CalculateStd($_, $WorkingHash->{$_}, $NextValue); }
		if($_ eq "del") 	{ $WorkingHash->{$_} = CalculateDel($_, $WorkingHash->{$_}, $NextValue); }		
	}
	my $BuildString = "";
	foreach (split(/:/,$Format)) {
		if($BuildString ne "") {
			$BuildString .= ":";
		}
		#print "$BuildString . ".$WorkingHash->{$_}."\n";
		$BuildString .= $WorkingHash->{$_};
	}
	#print "BuildString='$BuildString'\n";
	return($BuildString);
}

sub Preset {
	my $NextValue = shift @_;
	my %WorkingHash;
	# @WorkingHash{split(/:/,$Format)} = split(/:/,$ContainerRef);
	my @Fields = split(/:/,$Format);
	$WorkingHash->{"count"} = 1;
	$WorkingHash->{"sum"} = $NextValue;
	$WorkingHash->{"mean"} = $WorkingHash->{"sum"} / $WorkingHash->{"count"};

	foreach (@Fields) {
		if($_ eq "max") 	{ $WorkingHash->{$_} = $NextValue; }
		if($_ eq "min") 	{ $WorkingHash->{$_} = $NextValue; }
		if($_ eq "std") 	{ $WorkingHash->{$_} = $NextValue; }
		if($_ eq "del") 	{ $WorkingHash->{$_} = CalculateDel($_, $WorkingHash->{$_}, $NextValue); }
	}
	my $BuildString = "";
	foreach (split(/:/,$Format)) {
		if($BuildString ne "") {
			$BuildString .= ":";
		}
		$BuildString .= $WorkingHash->{$_};
	}
	#print "BuildString='$BuildString'\n";
	return($BuildString);
}

while ($#ARGV > -1) {
		my $Arg = shift @ARGV;
		if ($Arg eq "-s") { $TimeStepMinutes = shift @ARGV; }
		if ($Arg eq "-h") { ShowHelp(); }
		if ($Arg eq "-d") { $Debug = 1;}
		if ($Arg eq "-f") { $Format = shift @ARGV; }
}
my %Rows;
my @Sequence;
my $EndTime = DateTime->new(year=>0001)->truncate(to => 'day');
my $StartTime = $EndTime->clone;

# This forces the minutes to start at zero
my $ForceMinuteRule = 1; 

my $ETT;
while (<>) {
	@data = split(/\s+/);
	my $Nd = DateTime::Format::Strptime->new( pattern => '%H:%M:%S' )->parse_datetime( @data[0] );
	$Nd->set_second(0);
	if($Debug == 1) {
		print "\n$_\n";
		print "Nd datetime = ".$Nd->datetime();
		if(defined($EndTime)) {
			print "\t EndTime datetime = ".$EndTime->datetime();
		}
		print "\n";
	}
	if ( DateTime->compare($Nd, $EndTime) == 1) {
	 	$StartTime = $EndTime->clone();
		$EndTime = $StartTime->clone->add(minutes => $TimeStepMinutes);		
		if($Debug == 1) { print "StartTime datetime = ".$StartTime->datetime()."\t EndTime datetime = ".$EndTime->datetime()."\n"; }
		if ( $Nd > $EndTime) {
			$StartTime = DateTime::Format::Strptime->new( pattern => '%H:%M:%S' )->parse_datetime( @data[0] );
			$StartTime->set_second(0);
			while($StartTime->minute() % $TimeStepMinutes) {
				if($Debug == 1) { print "Step back time = ".$StartTime->datetime()."\n"; }	
				$StartTime->subtract(minutes => 1);
			}
			$EndTime = $StartTime->clone->add(minutes => $TimeStepMinutes);
			if($Debug == 1) { print "New EndTime on second try = ".$EndTime->datetime()."\n"; }
		}  else {
			if($Debug == 1) { print "New EndTime on first try = ".$EndTime->datetime()."\n"; }
		}
		shift @data;

		$ETT = $EndTime->strftime("%H:%M:%S");
		push(@Sequence, $ETT);
		$i = 0;
		my @Columns = ();
		#print "New array Columns @ ".\@Columns."\n";
		foreach (@data) {			
			$Columns[$i++] = Preset($_);			
		}
		$Rows{$ETT} = \@Columns;
	} else {
		$Columns = $Rows{$ETT};
		#print "Using Columns @ $Columns\n";
		shift @data;
		$i = 0;
		foreach (@data) {
			#print "update ".$$Columns[$i]."\n";
			$$Columns[$i++] = Calculate($_, $$Columns[$i]);
		}
	}
}

foreach (@Sequence) {
	print $_;
	$S = $_;
	foreach ( $Rows{$S} ) {
		my $Columns = $_;
		foreach (@{$Columns}) {
			print ",".$_;
		}
	}
	print "\n";
}
