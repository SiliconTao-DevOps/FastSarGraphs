{ 
	if ( $1 == "===========" ) { 
		nextfields=0
	} else {	
		if ( nextfields == 0) {
			split($0, tmp)
			for (i=2; i<=NF; i++) {
				fields[tmp[i]] = tmp[i];
			}
		} else {
			times[$1]=$1
			# printf("%s ", times[$1])
			for (i=2; i<=NF; i++) {
				record[$1, fields[tmp[i]]] = $(i);
				# printf("%s(%s) ", fields[tmp[i]], $(i))
			}
		}
		#print "\n"
		nextfields++
	}
} 

END{
	printf("sar_time ");
	for (fkey in fields) {
		printf("%10s ", fkey)
	}
	printf("\n")
	for (tkey in times) {
		printf("%s ", tkey)
		for (fkey in fields) {
			printf("%10s ", record[times[tkey], fields[fkey]])
	        }
		printf("\n")
	}
}
