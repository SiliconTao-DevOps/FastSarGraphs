#!/bin/bash

REPLACE_DAY=
REPLACE_START=
REPLACE_END=
DEBUG=


TODAY=$(echo $(date +%e))
SCAN_DAY=$(echo $(date +%e -d"2 hour ago"))
START_TIME=$(date +%H:%M:%S -d "2 hour ago")
END_TIME=$(date +%H:%M:%S -d "$START_TIME 2 hour")

if [ ! "${REPLACE_DAY}x" = "x" ]; then
	echo "Replace day with $REPLACE_DAY"
	SCAN_DAY=${REPLACE_DAY}
fi
if [ ! "${REPLACE_START}x" = "x" ]; then
	echo "Replace start with $REPLACE_START"
	START_TIME="${REPLACE_START}"
fi
if [ ! "${REPLACE_END}x" = "x" ]; then
	echo "Replace end with $REPLACE_END"
	END_TIME=${REPLACE_END}
fi

DAY_CMD=""
((DEBUG == 1)) && echo "Test date formating '$TODAY' = '$SCAN_DAY'"  >&2

if [ ! "$TODAY" = "$SCAN_DAY" ]; then
	DAY_CMD="-f /var/log/sa/sa$(printf "%02d" $SCAN_DAY)"
fi
[ $(date +%e) -gt $SCAN_DAY ] && {
	echo "DATE=$(date +%F -d"$(($(date +%e) - ${SCAN_DAY##0})) days ago")"
}
[ $(date +%e) -lt $SCAN_DAY ] && {
	echo "DATE=$(date +%Y-%m -d"last month")-$SCAN_DAY"
}
[ $(date +%e) -eq $SCAN_DAY ] && {
	echo "DATE=$(date +%F)"
}
echo "LC_TIME=C sar -s $START_TIME -e $END_TIME $DAY_CMD "
((DEBUG == 1)) && echo "LC_TIME=C sar -s $START_TIME -e $END_TIME $DAY_CMD " >&2
echo "BEGIN DATA"
# -u sar.CPU_utilization
# -r sar.RAM_utilization
# -q sar.queue_length
# "-I SUM" sar.interrupts
# -c sar.new_processes, not supported on all systems
# -w sar.task_switching
# -W sar.swapping
# -b sar.block
# -S CentOS 6 Report swap space utilization statistics.
TMP=$(mktemp /dev/shm/SAR_TMP_XXXXXXXXXXXXXXXXX)
for i in -u -r -q "-I SUM" -w -W -b -p -S; do
	((DEBUG == 1)) && echo "LC_TIME=C sar -s $START_TIME -e $END_TIME $DAY_CMD $i" >&2
	LC_TIME=C sar -s $START_TIME -e $END_TIME $DAY_CMD $i 2>/dev/null | grep ^[0-9] > $TMP
	echo "===========" >> $TMP
	grep -q ^[0-9] $TMP && cat $TMP
done
rm $TMP

FRONTSIDE_DEV=$(ip ro | grep default | tail -n 1|gawk '{print $NF}')
NETWORK_DEV=$(ip a|grep mq.master|sed -e 's/.*mq master //' | cut -d\  -f1|head -n1 | grep bond) || {
	NETWORK_DEV=$FRONTSIDE_DEV
}
(( DEBUG == 1)) && echo "NETWORK_DEV=$NETWORK_DEV" >&2
LC_TIME=C sar -s $START_TIME -e $END_TIME $DAY_CMD -n DEV 2>/dev/null | grep -E "^[0-9].*${NETWORK_DEV} |IFACE"
echo "==========="

LC_TIME=C sar -s $START_TIME -e $END_TIME $DAY_CMD -n NFSD 2>/dev/null | grep -E "^[0-9].*"
echo "==========="
